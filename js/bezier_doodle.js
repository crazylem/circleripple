/*
 * Copyright MIT © <2013> <Francesco Trillini>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
 * to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

var BezierDoodle = {};

;(function(self, undefined) {

    var canvas, context, nodes = [], ripples = [], mouse = { x: 0, y: 0 }, color = { r: 0, g: 0, b: 0 }, cycle = 90, input = false, FPS = 60;

    // Dat GUI default values
    var reactivity = 0.5, debug = false;

    /*
     * Settings.
     */

    var Settings = function() {

        this.reactivity = 0.5;
        this.debug = false;

        this.changeReactivity = function(value) {

            reactivity = value;

        };

        this.enableDebug = function(value) {

            debug = value;

        };

    };

    /*
     * Init.
     */

    function init() {

        var settings = new Settings();
        var GUI = new dat.GUI();

        // Dat GUI main
        GUI.add(settings, 'reactivity').min(0.1).max(0.5).onChange(settings.changeReactivity);
        GUI.add(settings, 'debug').onChange(settings.enableDebug);

        var body = document.querySelector('body');

        canvas = document.createElement('canvas');

        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;

        canvas.style.background = '-webkit-radial-gradient(#ffffff, #d1f1ff)';
        canvas.style.background = '-moz-radial-gradient(#ffffff, #d1f1ff)';
        canvas.style.background = '-ms-radial-gradient(#ffffff, #d1f1ff)';
        canvas.style.background = '-o-radial-gradient(#ffffff, #d1f1ff)';
        canvas.style.background = 'radial-gradient(#ffffff, #d1f1ff)';

        canvas.style.position = 'absolute';
        canvas.style.top = 0;
        canvas.style.bottom = 0;
        canvas.style.left = 0;
        canvas.style.right = 0;
        canvas.style.zIndex = -1;
        canvas.style.cursor = 'pointer';

        body.appendChild(canvas);

        if(!!(capable)) {

            context = canvas.getContext('2d');

            // Events
            if('ontouchstart' in window) {

                canvas.addEventListener('touchstart', onTouchStart, false);
                canvas.addEventListener('touchend', onTouchEnd, false);

            }

            else {

                canvas.addEventListener('mousedown', onMouseDown, false);
                canvas.addEventListener('mouseup', onMouseUp, false);

            }

            window.onresize = onResize;

            createBezierNodes();

        }

    }

    /*
     * Check if browser supports canvas element.
     */

    function capable() {

        return canvas.getContext && canvas.getContext('2d');

    }

    /*
     * On resize window event.
     */

    function onResize() {

        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;

    }

    /*
     * Mouse down event.
     */

    function onMouseDown(event) {

        event.preventDefault();

        mouse.x = event.pageX - canvas.offsetLeft;
        mouse.y = event.pageY - canvas.offsetTop;

        addRipple();

    }

    /*
     * Mouse up event.
     */

    function onMouseUp(event) {

        event.preventDefault();

        input = false;

    }

    /*
     * Touch start event.
     */

    function onTouchStart(event) {

        event.preventDefault();

        mouse.x = event.touches[0].pageX - canvas.offsetLeft;
        mouse.y = event.touches[0].pageY - canvas.offsetTop;

        addRipple();

    }

    /*
     * Touch end event.
     */

    function onTouchEnd(event) {

        event.preventDefault();

        input = false;

    }

    /*
     * Let's create the basic skin of bezier curve.
     */

    function createBezierNodes() {

        updateColorCycle();

        for(var quantity = 0, len = 6; quantity < len; quantity++) {

            var theta = Math.PI * 2 * quantity / len;

            var x = canvas.width * 0.5 + 0 * Math.cos(theta);
            var y = canvas.height * 0.5 + 0 * Math.sin(theta);

            nodes.push({

                x: x,
                y: y,
                vx: 0,
                vy: 0,

                lastX: x,
                lastY: y,

                min: 150,
                max: 250,
                disturb: 150,

                orbit: 20,
                angle: Math.random() * Math.PI * 2,
                speed: 0.05 + Math.random() * 0.05,

                theta: theta

            });

        }

        loop();

    }

    /*
     * Add a ripple at every input.
     */

    function addRipple() {

        input = true;

        if(ripples.length === 0) {

            updateColorCycle();

            ripples.push({

                x: mouse.x,
                y: mouse.y,

                reactivity: 0,
                fade: 1.0

            });

        }

    }

    /*
     * Loop logic.
     */

    function loop() {

        clear();
        update();
        render();

        requestAnimFrame(loop);

    }

    /*
     * Clear the whole screen.
     */

    function clear() {

        context.clearRect(0, 0, innerWidth, innerHeight);

    }

    /*
     * Update the nodes.
     */

    function update() {

        var ease = 0.01, friction = 0.98;

        for(var index = 0; index < ripples.length; index++) {

            var ripple = ripples[index];

            ripple.reactivity += 5;
            ripple.fade -= 0.05;

            if(ripple.fade <= 0.0)

                ripples.splice(index, 1);

        }

        [].forEach.call(nodes, function(node, index) {

            context.clearRect(0, 0, canvas.width, canvas.height);

            node.lastX += (canvas.width * 0.5 + node.disturb * Math.cos(node.theta) - node.lastX) * 0.08;
            node.lastY += (canvas.height * 0.5 + node.disturb * Math.sin(node.theta) - node.lastY) * 0.08;

            // Motion
            node.x += ((node.lastX + Math.cos(node.angle) * node.orbit) - node.x) * 0.08;
            node.y += ((node.lastY + Math.sin(node.angle) * node.orbit) - node.y) * 0.08;

            // Ease
            node.vx += (node.min - node.disturb) * ease;

            // Friction
            node.vx *= friction;

            node.disturb += node.vx;

            if(input)

                node.disturb += (node.max - node.disturb) * reactivity;

            node.angle += node.speed;

        });

    }

    /*
     * Render the nodes.
     */

    function render() {

        var currentIndex, nextIndex, xc, yc;

        [].forEach.call(nodes, function(node, index) {

            clear();

            currentIndex = nodes[nodes.length - 1];
            nextIndex = nodes[0];

            xc = currentIndex.x + (nextIndex.x - currentIndex.x) * 0.5;
            yc = currentIndex.y + (nextIndex.y - currentIndex.y) * 0.5;

            context.save();
            context.strokeStyle = debug ? '#a9a9a9' : '#e5e5e5';
            context.fillStyle = 'rgb' + '(' + color.r + ', ' + color.g + ', ' + color.b + ')';
            context.globalAlpha = debug ? 1.0 : 0.5;
            context.lineWidth = debug ? 1 : 5;
            context.beginPath();
            context.moveTo(xc, yc);

            // Draw through N points
            for(var N = 0; N < nodes.length; N++) {

                currentIndex = nodes[N];
                nextIndex = N + 1 > nodes.length - 1 ? nodes[N - nodes.length + 1] : nodes[N + 1];

                xc = currentIndex.x + (nextIndex.x - currentIndex.x) * 0.5;
                yc = currentIndex.y + (nextIndex.y - currentIndex.y) * 0.5;

                context.quadraticCurveTo(currentIndex.x, currentIndex.y, xc, yc);

            }

            debug ? null : context.fill();
            context.stroke();
            context.restore();

            context.save();
            context.globalAlpha = 1.0;
            context.lineWidth = 1;
            context.lineCap = 'round';
            context.lineJoin = 'round';
            context.strokeStyle = '#a9a9a9';
            context.fillStyle = '#a9a9a9';

            // Draw through N points
            for(var N = 0; N < nodes.length; N++) {

                // First anchor
                currentIndex = nodes[N];
                nextIndex = N + 1 > nodes.length - 1 ? nodes[N - nodes.length + 1] : nodes[N + 1];

                xc = currentIndex.x + (nextIndex.x - currentIndex.x) * 0.8;
                yc = currentIndex.y + (nextIndex.y - currentIndex.y) * 0.8;

                context.beginPath();
                context.moveTo(xc, yc);

                // Second anchor
                currentIndex = nextIndex;
                nextIndex = N + 2 > nodes.length - 1 ? nodes[N - nodes.length + 2] : nodes[N + 2];

                xc = currentIndex.x + (nextIndex.x - currentIndex.x) * 0.2;
                yc = currentIndex.y + (nextIndex.y - currentIndex.y) * 0.2;

                context.lineTo(xc, yc);
                context.stroke();

                // First anchor
                currentIndex = nodes[N];
                nextIndex = N + 1 > nodes.length - 1 ? nodes[N - nodes.length + 1] : nodes[N + 1];

                xc = currentIndex.x + (nextIndex.x - currentIndex.x) * 0.8;
                yc = currentIndex.y + (nextIndex.y - currentIndex.y) * 0.8;

                context.beginPath();
                context.arc(xc, yc, 2, 0, Math.PI * 2);
                context.fill();

                // Second anchor
                currentIndex = nextIndex;
                nextIndex = N + 2 > nodes.length - 1 ? nodes[N - nodes.length + 2] : nodes[N + 2];

                xc = currentIndex.x + (nextIndex.x - currentIndex.x) * 0.2;
                yc = currentIndex.y + (nextIndex.y - currentIndex.y) * 0.2;

                context.beginPath();
                context.arc(xc, yc, 2, 0, Math.PI * 2);
                context.fill();

            }

            context.restore();

        });

        for(var index = 0; index < ripples.length; index++) {

            var ripple = ripples[index];

            context.globalAlpha = ripple.fade;
            context.fillStyle = '#ffffff';
            context.beginPath();
            context.arc(ripple.x, ripple.y, ripple.reactivity, 0, Math.PI * 2);
            context.closePath();
            context.fill();

        }

    }

    /*
     * Update color cycle.
     */

    function updateColorCycle() {

        cycle = Math.min(cycle + reactivity + 0.3, 100) !== 100 ? cycle += reactivity + 0.3 : 0;

        color.r = ~~(Math.sin(0.3 * cycle + 0) * 127 + 128);
        color.g = ~~(Math.sin(0.3 * cycle + 2) * 127 + 128);
        color.b = ~~( Math.sin(0.3 * cycle + 4) * 127 + 128);

    }

    window.requestAnimFrame = (function() {

        return  window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            window.oRequestAnimationFrame      ||
            window.msRequestAnimationFrame     ||

            function(callback) {

                window.setTimeout(callback, 1000 / FPS);

            };

    })();

    window.addEventListener ? window.addEventListener('load', init, false) : window.onload = init;

})(BezierDoodle);
"use strict";

var canvas = document.getElementById('myCanvas');

var debug = false;

var circleRippleParams = {
    canvas: canvas,
    centerX: 100,
    centerY: 100,
    segmentsAmount: 20,
    radius: {
        base: 50,
        max: 0,
        min: 0
    },
    radiusDivergence: 2,
    velocityBase: 0.5,
    mouse: {
        maxRadiusDiffToReact: 50,
        maxAngleToReact: 50,
        velocityModifierRatio: 0.1,
        windRatio: 0.5,
        windFadeRatio: 0.05,
        windShiftMax: 10,
        windSpeed: 1
    },
    fps: 20
};
var myRipple = new CircleRipple(circleRippleParams);
myRipple.start();

function CircleRipple(params) {
    var lastTime, context, angleOffset, nodes, mouse;

    initialize();

    this.start = function() {
        setMouseEvents();
        requestAnimationFrame(loop);
    };

    function initialize() {
        lastTime = 0;
        context = params.canvas.getContext('2d');
        params.radius.min = params.radius.base - params.radiusDivergence;
        params.radius.max = params.radius.base + params.radiusDivergence;
        angleOffset = 360 / params.segmentsAmount;

        nodes = [];
        for (var i = 0, angle = 0; i < params.segmentsAmount; i++, angle += angleOffset) {
            var node = new Node(angle);
            nodes.push(node);
        }

        mouse = new Mouse();
    }

    function getRandom(from, to) {
        return Math.random() * (to - from) + from;
    }

    function setMouseEvents() {
        canvas.addEventListener('mousemove', onMouseMove);
    }

    function onMouseMove(event) {
        var currentTime = performance.now();
        if (currentTime - mouse.lastMoveTime < 50) {
            return;
        }
        mouse.lastMoveTime = currentTime;
        if ((! mouse.configured) && ++mouse.eventsCount === 2 ) {
            mouse.configured = true;
        }
        mouse.x1 = mouse.x2;
        mouse.y1 = mouse.y2;
        mouse.x2 = event.clientX - params.centerX;
        mouse.y2 = event.clientY - params.centerY;
        mouse.moved = true;
    }

    function getRadians(degree) {
        return degree/180*Math.PI;
    }

    function getX(radius, angle) {
        return radius * Math.cos(getRadians(angle));
    }

    function getY(radius, angle) {
        return radius * Math.sin(getRadians(angle));
    }

    function drawPoint(x, y, width, fillStyle) {
        context.fillStyle = fillStyle;
        context.fillRect(x - width/2 + params.centerX, y - width/2 + params.centerY, width, width);
    }

    function drawCircle(x, y, radius) {
        context.strokeStyle = 'blue';
        context.beginPath();
        context.arc(x + params.centerX, y + params.centerY, radius, 0, getRadians(360));
        context.stroke();
    }

    function drawCurve(i) {
        debug && drawPoint(nodes[i].x, nodes[i].y, 2, 'red');
        var prev = getNeighbor(i, -1, params.segmentsAmount);
        context.bezierCurveTo(
            nodes[prev].bezierRightX + params.centerX,
            nodes[prev].bezierRightY + params.centerY,
            nodes[i].bezierLeftX + params.centerX,
            nodes[i].bezierLeftY + params.centerY,
            nodes[i].x + params.centerX,
            nodes[i].y + params.centerY
        );
    }

    function drawLine(x1, y1, x2, y2, strokeStyle) {
        context.strokeStyle = strokeStyle;
        context.beginPath();
        context.moveTo(x1 + params.centerX, y1 + params.centerY);
        context.lineTo(x2 + params.centerX, y2 + params.centerY);
        context.stroke();
        context.closePath()
    }

    function getNeighbor(current, increment, max) {
        var neighbor = current + increment;
        if (neighbor >= max) {
            neighbor -= max;
        }
        if (neighbor < 0) {
            neighbor += max;
        }
        return neighbor;
    }

    function loop(time) {
        if ((time - lastTime) / 1000 > 1 / params.fps) {
            clear();
            handleNodes();
            render();
            lastTime = time;
        }
        window.requestAnimationFrame(loop);
    }

    function clear() {
        context.clearRect(0, 0, window.innerHeight, window.innerWidth);
    }

    function render() {
        if (debug) {
            drawPoint(0, 0, 4, 'blue');
            // mouse.configured && drawLine(mouse.x1, mouse.y1, mouse.x2, mouse.y2, 'green');
        }

        context.strokeStyle = 'black';
        context.beginPath();
        context.moveTo(nodes[0].x + params.centerX, nodes[0].y + params.centerY);
        for (var i = 1; i < params.segmentsAmount; i += 1) {
            drawCurve(i);
        }
        drawCurve(0);
        context.stroke();
    }

    function getHypotenuse(c1, c2) {
        return Math.sqrt(Math.pow(c1, 2) + Math.pow(c2, 2));
    }

    /**
     * Возвращает коэффициенты для уравнения прямой y = k * x + b
     * @param coordinates
     * @returns {{k: number, b: number}}
     */
    function getLineEquation(coordinates) {
        var k = (coordinates.y2- coordinates.y1) / (coordinates.x2 - coordinates.x1);
        var b = coordinates.y1 - k * coordinates.x1;
        return {k: k, b: b};
    }

    /**
     * Возвращает расстояние от начала координат до прямой проходящей через точку (px, py)
     * и паралельной другой прямой с формулой y = kx + b
     * @param {number} k
     * @param {number} px
     * @param {number} py
     * @returns {number}
     */
    function getDistanceToLine(k, px, py) {
        if (k === Number.POSITIVE_INFINITY || k === Number.NEGATIVE_INFINITY) {
            return px;
        }
        var alpha = Math.atan(k);
        return (py - k * px) * Math.cos(alpha);
    }

    function getVelocityModifier(radius, mode) {
        var difference = Math.abs(radius.center - params.radius.base);
        if (mode === 'bySpeed') {

        }
        return 1 + difference * params.mouse.velocityModifierRatio;
    }

    /**
     * Обрабатывает смещение (ветер) зависящий от движения мыши. Учитывает направление и скорость
     * @param node
     */
    function handleWind(node) {
        if (! mouse.configured) {
            return;
        }
        if (! mouse.moved) {
            if (Math.abs(node.wind.x) > Math.abs(node.wind.to.x)) {
                node.wind.to.x = 0;
                node.wind.to.y = 0;
                node.wind.speed.x = -node.wind.x * params.mouse.windFadeRatio;
                node.wind.speed.y = -node.wind.y * params.mouse.windFadeRatio;
            }
            node.wind.x += node.wind.speed.x;
            node.wind.y += node.wind.speed.y;
            return;
        }

        var xMax = Math.max(mouse.x1, mouse.x2) + params.mouse.maxRadiusDiffToReact;
        var xMin = Math.min(mouse.x1, mouse.x2) - params.mouse.maxRadiusDiffToReact;
        var yMax = Math.max(mouse.y1, mouse.y2) + params.mouse.maxRadiusDiffToReact;
        var yMin = Math.min(mouse.y1, mouse.y2) - params.mouse.maxRadiusDiffToReact;
        if (! (xMax > node.x && xMin < node.x && yMax > node.y && yMin < node.y)) {
            return;
        }

        var lineEquation = getLineEquation(mouse);
        var distance = getDistanceToLine(lineEquation.k, mouse.x2, mouse.y2);
        var xBase = getX(params.radius.base, node.angle);
        var yBase = getY(params.radius.base, node.angle);
        var distanceBase = getDistanceToLine(lineEquation.k, xBase, yBase);
        var difference = Math.abs(distance - distanceBase);
        if (difference >= params.mouse.maxRadiusDiffToReact) {
            return;
        }

        var fadeRatio = (params.mouse.maxRadiusDiffToReact - difference) / params.mouse.maxRadiusDiffToReact;
        var windX = (mouse.x2 - mouse.x1) * fadeRatio * params.mouse.windRatio;
        var windY = (mouse.y2 - mouse.y1) * fadeRatio * params.mouse.windRatio;
        if (Math.abs(windX) > params.mouse.windShiftMax) {
            windX = windX > 0 ? params.mouse.windShiftMax : -params.mouse.windShiftMax
        }
        if (Math.abs(windY) > params.mouse.windShiftMax) {
            windY = windY > 0 ? params.mouse.windShiftMax : -params.mouse.windShiftMax
        }

        node.wind.speed.x = windX * params.mouse.windFadeRatio;
        node.wind.speed.y = windY * params.mouse.windFadeRatio;
        node.wind.to.x = windX;
        node.wind.to.y = windY;
    }

    function handleNodes() {
        debug && drawCircle(0, 0, params.radius.base);
        if (debug && mouse.configured ) {
            drawLine(mouse.x1, mouse.y1, mouse.x2, mouse.y2, 'green');
        }

        nodes.forEach(function(node) {
            handleWind(node);
            // var radius = mouse.getRadius(node.angle);
            var radius = params.radius;
            // var vm = getVelocityModifier(radius);
            var vm = 1;
            if (radius.min > node.radius) {
                node.velocity = params.velocityBase * vm;
            }
            if (radius.max < node.radius) {
                node.velocity = -params.velocityBase * vm;
            }
            node.radius += node.velocity;
        });
        mouse.moved = false;
    }

    function Node(angle) {
        var _radius = {
            length: 0,
            x: 0,
            y: 0
        };
        var _bezierControlPoints = {
            left: {x: 0, y: 0},
            right: {x: 0, y: 0}
        };

        // setRadius(params.radius.base + getRandom(-params.radiusDivergence, params.radiusDivergence));
        var sinForRandom = Math.sin(angle * 5 * getRandom(1, 1.1) / 180 * Math.PI + 0.5);
        setRadius(params.radius.base + sinForRandom * params.radiusDivergence);
        this.angle = angle;
        this.velocity = params.velocityBase * sinForRandom;
        this.wind = {x: 0, y: 0,
            to: {x: 0, y: 0},
            speed: {x: 0, y: 0}
        };

        Object.defineProperty(this, 'radius', {
            set: function(radius) {
                setRadius(radius);
            },
            get: function() {
                return _radius.length;
            }
        });

        Object.defineProperty(this, 'x', {
            get: function() {
                return _radius.x + this.wind.x;
            }
        });

        Object.defineProperty(this, 'y', {
            get: function() {
                return _radius.y + this.wind.y;
            }
        });

        Object.defineProperty(this, 'bezierLeftX', {
            get: function() {
                return _bezierControlPoints.left.x + this.wind.x;
            }
        });

        Object.defineProperty(this, 'bezierLeftY', {
            get: function() {
                return _bezierControlPoints.left.y + this.wind.y;
            }
        });

        Object.defineProperty(this, 'bezierRightX', {
            get: function() {
                return _bezierControlPoints.right.x + this.wind.x;
            }
        });

        Object.defineProperty(this, 'bezierRightY', {
            get: function() {
                return _bezierControlPoints.right.y + this.wind.y;
            }
        });

        function setRadius(radius) {
            _radius.length = radius;
            _radius.x = getX(_radius.length, angle);
            _radius.y = getY(_radius.length, angle);

            _bezierControlPoints.left.x = getX(_radius.length, angle - angleOffset/3);
            _bezierControlPoints.left.y = getY(_radius.length, angle - angleOffset/3);
            _bezierControlPoints.right.x = getX(_radius.length, angle + angleOffset/3);
            _bezierControlPoints.right.y = getY(_radius.length, angle + angleOffset/3);
        }
    }

    function Mouse() {
        var opposite, adjacent;

        this.configured = false;
        this.eventsCount = 0;
        this.lastMoveTime = performance.now();

        this.x1 = 0;
        this.y1 = 0;
        this.x2 = 0;
        this.y2 = 0;
        this.moved = false;

        this.getRadius = function(currentAngle) {
            this.fillCatheters();

            // adjacent = 0;
            // mouse.configured = true;

            var radius = params.radius;
            // radius.min = params.radius.min;
            // radius.max = params.radius.max;
            // radius.base = params.radius.base;
            if (! mouse.configured) {
                return radius;
            }
            var mouseRadius = getHypotenuse(adjacent, opposite);
            var radiusDifference = mouseRadius - params.radius.base;
            if (radiusDifference < params.mouse.maxRadiusDiffToReact) {
                debug && drawPoint(this.x2, this.y2, 4, 'red');
                debug && drawPoint(this.x1, this.y1, 4, 'red');
                var angleDifference = Math.abs(getNeighbor(getAngle(), 0, 360) - currentAngle);
                var maxAngle = params.mouse.maxAngleToReact;
                if (angleDifference < maxAngle) {
                    var newRadius =  (maxAngle - angleDifference) / maxAngle * (mouseRadius - params.radius.base) + params.radius.base;
                    radius.min = newRadius - params.radiusDivergence;
                    radius.max = newRadius + params.radiusDivergence;
                    radius.base = newRadius;
                }
            }
            return radius;
        };

        this.fillCatheters = function() {
            opposite = this.y2;
            adjacent = this.x2;
        };

        function getAngle() {
            var mouseAngle;
            if (adjacent === 0) {
                mouseAngle = opposite > 0 ? 90 : 270;
            } else {
                mouseAngle = Math.atan(opposite/adjacent) * 180 / Math.PI;
            }
            if (opposite < 0  && adjacent > 0) {
                mouseAngle += 360;
            }
            if (adjacent < 0) {
                mouseAngle += 180;
            }
            // mouseAngle = opposite < 0  && adjacent > 0 ?  360 + mouseAngle : mouseAngle;
            // mouseAngle = adjacent < 0 ?  180 + mouseAngle: mouseAngle;
            return mouseAngle;
        }
    }
}

